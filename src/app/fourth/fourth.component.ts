import {Component, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subscription} from "rxjs";
import {map, pairwise} from "rxjs/operators";

// CRED: Highly inspired by
// Lukas Ruebbelke: Go beast mode with realtime reactive interfaces in Angular 2 & Firebase
// https://www.youtube.com/watch?v=5CTL7aqSvJU
@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.css']
})
export class FourthComponent implements OnInit, OnDestroy {
  lines: any[] = [];

  private subscription: Subscription = new Subscription();


  ngOnInit() {
    this.subscription.add(fromEvent(document, 'mousemove')
      .pipe(
        map((e: MouseEvent) => {
          return {x: e.pageX, y: e.pageY}
        }),
        pairwise(),
        map(postitions => {
          const p1 = postitions[0];
          const p2 = postitions[1];
          return {x1: p1.x, y1: p1.y, x2: p2.x, y2: p2.y};
        })
      ).subscribe(line => this.lines = [...this.lines, line]));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }



}
