import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {fromEvent, merge, Subscription} from "rxjs";
import {map, scan, startWith} from "rxjs/operators";

// CRED: Highly inspired by
// Lukas Ruebbelke: Go beast mode with realtime reactive interfaces in Angular 2 & Firebase
// https://www.youtube.com/watch?v=5CTL7aqSvJU
@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements AfterViewInit, OnDestroy {
  @ViewChild("right", {static: false}) right: ElementRef;
  @ViewChild("left", {static: false}) left: ElementRef;

  private subscription: Subscription = new Subscription();

  x: number = 50;

  ngAfterViewInit(): void {
    const right$ = fromEvent(this.right.nativeElement, 'click')
      .pipe(
        map(e => 10)
      );

    const left$ = fromEvent(this.left.nativeElement, 'click')
      .pipe(
        map(e => -10)
      );

    this.subscription.add(merge(right$, left$)
      .pipe(
        startWith(50),
        scan((acc, curr) => {
          return acc + curr
        })
      ).subscribe(result => this.x = result));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
