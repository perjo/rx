import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';

// CRED: Highly inspired by
// Lukas Ruebbelke: Go beast mode with realtime reactive interfaces in Angular 2 & Firebase
// https://www.youtube.com/watch?v=5CTL7aqSvJU
@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements AfterViewInit, OnDestroy {

  private subscription: Subscription = new Subscription();

  @ViewChild("magicButton", {static: false}) magicButton: ElementRef;

  ngAfterViewInit() {
    const observable: Observable<MouseEvent> = fromEvent(this.magicButton.nativeElement, 'click');

    this.subscription.add(observable
      .pipe(
        filter(e => e.shiftKey)
      )
      .subscribe(e => console.log('Observable magic!')));

  }

  doCallbackMagic() {
    console.log('Callback magic!');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
