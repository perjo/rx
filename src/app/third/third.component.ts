import {Component, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subscription} from "rxjs";
import {map, startWith, switchMap, takeUntil} from "rxjs/operators";

// CRED: Highly inspired by
// Lukas Ruebbelke: Go beast mode with realtime reactive interfaces in Angular 2 & Firebase
// https://www.youtube.com/watch?v=5CTL7aqSvJU
@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit, OnDestroy {
  position = {x: 30, y: 30};

  private subscription: Subscription = new Subscription();

  ngOnInit() {
    const move$ = fromEvent(document, 'mousemove')
      .pipe(
        map((e: MouseEvent) => {
          return {x: e.pageX, y: e.pageY};
        }));


    const down$ = fromEvent(document, 'mousedown');
    const up$ = fromEvent(document, 'mouseup');

    this.subscription.add(down$.pipe(
      switchMap(e => move$.pipe(
        takeUntil(up$)
      )),
      startWith({x: 100, y: 100})
    ).subscribe(result => this.position = result));

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
